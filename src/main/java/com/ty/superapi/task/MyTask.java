package com.ty.superapi.task;

import org.springframework.scheduling.annotation.Scheduled;

/**
 * 任务
 * <p>Created on 2017/9/28</p>
 *
 * @author tianyu
 */
//@Component
public class MyTask {

    @Scheduled(cron = "*/5 * * * * ?")
    public void show() {
        System.out.println("我是任务");
    }
}
