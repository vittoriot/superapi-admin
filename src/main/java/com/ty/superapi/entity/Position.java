package com.ty.superapi.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author tianyu
 * @since 2017-12-22
 */
public class Position extends Model<Position> {

    private static final long serialVersionUID = 1L;

	private Integer id;
    /**
     * 姓名
     */
	private String name;
    /**
     * 描述
     */
	private String description;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Position{" +
			"id=" + id +
			", name=" + name +
			", description=" + description +
			"}";
	}
}
