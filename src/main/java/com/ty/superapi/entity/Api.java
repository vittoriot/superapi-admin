package com.ty.superapi.entity;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author tianyu
 * @since 2017-12-27
 */
public class Api extends Model<Api> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 项目id
     */
    @TableField("project_id")
    private Integer projectId;
    /**
     * 所属模块id
     */
    private Integer pid;
    /**
     * 接口名称
     */
    private String name;
    /**
     * 接口路径
     */
    private String url;
    /**
     * 描述
     */
    private String description;
    /**
     * 备注
     */
    private String remark;
    /**
     * 请求类型
     */
    @TableField("req_type")
    private String reqType;
    /**
     * 请求参数
     */
    @TableField("req_param")
    private String reqParam;
    /**
     * 请求头
     */
    @TableField("req_header")
    private String reqHeader;
    /**
     * 响应数据
     */
    @TableField("resp_data")
    private String respData;
    /**
     * 创建人
     */
    private String creater;
    /**
     * mock值
     */
    @TableField("mock_value")
    private String mockValue;
    /**
     * mock表达式
     */
    @TableField("mock_rule")
    private String mockRule;
    /**
     * 是否废弃
     */
    private Integer discard;
    /**
     * 修改日期
     */
    @TableField("update_date")
    @JSONField(format = "yyyy-MM-dd hh:mm:ss")
    private Date updateDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public String getReqParam() {
        return reqParam;
    }

    public void setReqParam(String reqParam) {
        this.reqParam = reqParam;
    }

    public String getReqHeader() {
        return reqHeader;
    }

    public void setReqHeader(String reqHeader) {
        this.reqHeader = reqHeader;
    }

    public String getRespData() {
        return respData;
    }

    public void setRespData(String respData) {
        this.respData = respData;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public String getMockValue() {
        return mockValue;
    }

    public void setMockValue(String mockValue) {
        this.mockValue = mockValue;
    }

    public String getMockRule() {
        return mockRule;
    }

    public void setMockRule(String mockRule) {
        this.mockRule = mockRule;
    }

    public Integer getDiscard() {
        return discard;
    }

    public void setDiscard(Integer discard) {
        this.discard = discard;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Api{" +
                "id=" + id +
                ", projectId=" + projectId +
                ", pid=" + pid +
                ", name=" + name +
                ", url=" + url +
                ", description=" + description +
                ", remark=" + remark +
                ", reqType=" + reqType +
                ", reqParam=" + reqParam +
                ", reqHeader=" + reqHeader +
                ", respData=" + respData +
                ", creater=" + creater +
                ", mockValue=" + mockValue +
                ", mockRule=" + mockRule +
                ", discard=" + discard +
                ", updateDate=" + updateDate +
                "}";
    }
}
