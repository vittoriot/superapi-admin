package com.ty.superapi.controller;


import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ty.superapi.entity.Api;
import com.ty.superapi.service.ApiService;
import com.ty.superapi.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
@RestController
@RequestMapping("/api")
public class ApiController {


    @Autowired
    private ApiService apiService;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(Integer id) {
        // TODO 历史版本选择
        EntityWrapper<Api> entityWrapper = new EntityWrapper<Api>();
        entityWrapper.eq("project_id", id);
        List<Api> list = apiService.selectList(entityWrapper);
        return Result.success(list);
    }

    /**
     * 新增或修改api
     * @param api
     * @return
     */
    @RequestMapping(value = "put", method = RequestMethod.POST)
    public String put(@Valid Api api) {
        apiService.put(api);
        return Result.success();
    }

    /**
     * 删除api
     * @param id
     * @return
     */
    @RequestMapping(value = "del", method = RequestMethod.GET)
    public String del(Integer id) {
        // TODO 改逻辑删除
        apiService.deleteById(id);
        return Result.success();
    }

}
