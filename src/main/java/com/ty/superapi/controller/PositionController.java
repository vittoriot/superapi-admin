package com.ty.superapi.controller;


import com.baomidou.mybatisplus.mapper.Condition;
import com.ty.superapi.entity.Position;
import com.ty.superapi.entity.User;
import com.ty.superapi.service.PositionService;
import com.ty.superapi.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
@RestController
@RequestMapping("/position")
public class PositionController {

    @Autowired
    private PositionService positionService;

    /**
     * 查询职位列表
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list() {
        List<Position> list = positionService.selectList(Condition.EMPTY);
        return Result.success(list);
    }
}
