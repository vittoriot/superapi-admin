package com.ty.superapi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
@Controller
@RequestMapping("//userProject")
public class UserProjectController {
	
}
