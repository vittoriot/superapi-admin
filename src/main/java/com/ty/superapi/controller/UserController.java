package com.ty.superapi.controller;


import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ty.superapi.entity.User;
import com.ty.superapi.service.UserService;
import com.ty.superapi.util.Result;
import com.ty.superapi.util.StatusCode;
import com.xiaoleilu.hutool.crypto.SecureUtil;
import com.xiaoleilu.hutool.util.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 查询用户列表
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list() {
        List<User> list = userService.selectList(Condition.EMPTY);
        SimplePropertyPreFilter filter = Result.excludes("password", "createDate", "positionId");
        return Result.success(list, filter);
    }

    /**
     * 查询用户信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "get/{id}", method = RequestMethod.GET)
    public String get(@PathVariable Integer id) {
        User user = userService.selectById(id);
        return Result.success(user);
    }

    /**
     * 登录
     *
     * @param name     用户名
     * @param password 密码
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(String name, String password) {
        EntityWrapper<User> entityWrapper = new EntityWrapper<User>();
        entityWrapper.eq("name", name);
        entityWrapper.eq("password", SecureUtil.md5(password));
        User user = userService.selectOne(entityWrapper);
        if (ObjectUtil.isNotNull(user)) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("token", userService.encryptToken(user.getId() + "&" + user.getName()));
            map.put("name", user.getName());
            map.put("id", user.getId());
            // 项目经理
            if (user.getPositionId() == 8) {
                map.put("pm", true);
            }
            return Result.success(map);
        } else {
            return Result.error(StatusCode._1014);
        }

    }

    /**
     * 注册
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "regist", method = RequestMethod.POST)
    public String regist(@Valid User user) {
        EntityWrapper<User> entityWrapper = new EntityWrapper<User>();
        entityWrapper.eq("name", user.getName());
        User hasUser = userService.selectOne(entityWrapper);
        if (hasUser.getId() != 0) {
            return Result.error(StatusCode._1011);
        } else {
            userService.put(user);
            return Result.success();
        }
    }

}
